import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  Route,
  Path,
} from "tsoa";
import { Project } from "./interfaces";

import { ProjectsService } from "./ProjectsService";

@Route("projects")
export class ProjectsController extends Controller {
  @Get("")
  public async getProjects(): Promise<Project[]> {
    return new ProjectsService().get();
  }

  @Get("/{projectId}/allowed-reviewers") 
  public async getAssignedReviewers(
    @Path() projectId: number
  ) {
    return new ProjectsService().getReviewers(projectId)
  }

  @Post("/{projectId}/allowed-reviewers")
  public async addReviewer(
    @Body() requestBody: any,
    @Path() projectId: number
  ) {
    return new ProjectsService().addReviewer(projectId, requestBody.reviewerId, requestBody.name)
  }

  @Delete("/{projectId}/allowed-reviewers/{reviewerId}")
  public async removeReviewer(
    @Path() projectId: number,
    @Path() reviewerId: number
  ) {
    return new ProjectsService().removeReviewer(projectId, reviewerId)
  }
}
