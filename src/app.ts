import Koa, { Context, Next } from "koa";
import koaSwagger from "koa2-swagger-ui";
import Router from "@koa/router";
import bodyParser from "koa-bodyparser";
import { RegisterRoutes } from "./routes";

export const app = new Koa();
const router = new Router()

// Use body parser to read sent json payloads
app.use(bodyParser());

RegisterRoutes(router);

app.use(router.routes())
app.use(router.allowedMethods())

if (process.env.NODE_ENV === "development") {
  const docsMiddleware = async (ctx: Context, next: Next) => {
    const spec = await import("./swagger.json");
    return koaSwagger({ routePrefix: false, swaggerOptions: { spec } })(
      ctx,
      next
    );
  };

  router.get("/docs", docsMiddleware);
}