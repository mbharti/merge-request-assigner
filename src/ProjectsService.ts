import axios from "axios";
import { Project } from "./interfaces"

const BASE_URL = "https://gitlab.com/api/v4/users/mbharti"

interface Reviewer {
  projectId: number;
  id: number;
  name: string;
}

let assignedReviewers: Reviewer[] = [];

export class ProjectsService {
  public async get(): Promise<Project[]> {
   const response = await axios.get(`${BASE_URL}/projects`)
   return response.data.map(this.toProject)
  }

  toProject(res: any): Project {
    const { data } = res
    const { web_url } = data

    return {
      id: data.project_id,
      name: web_url
    }
  }

  public async getReviewers(projectId: number): Promise<Reviewer[]> {
    return assignedReviewers.filter(assignedReviewer => assignedReviewer.projectId === projectId);
  } 

  public async addReviewer(projectId: number, reviewerId: number, name: string): Promise<Reviewer[]> {
    assignedReviewers.push({
      projectId,
      id: reviewerId,
      name
    })

    return assignedReviewers;
  }

  public async removeReviewer(projectId: number, reviewerId: number): Promise<Reviewer[]> {
    assignedReviewers = assignedReviewers.filter(assignedReviewer => !(assignedReviewer.id === reviewerId && assignedReviewer.projectId === projectId))

    return assignedReviewers;
  }
}  