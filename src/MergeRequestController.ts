import {
  Body,
  Controller,
  Get,
  Post,
  Route,
} from "tsoa";

import { MergeRequestService } from "./MergeRequestService";

@Route("merge-requests")
export class MergeRequestController extends Controller {
  @Get("")
  public async getMergeRequest(): Promise<String> {
    return new MergeRequestService().get();
  }

  @Post("")
  public async mergeRequestEvent(
    @Body() requestBody: any
  ): Promise<void> {
    const mergeRequestId = new MergeRequestService().assignReviewer(requestBody);
    this.setStatus(201);
    return mergeRequestId;
  }
}
