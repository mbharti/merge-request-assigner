import axios from 'axios'

const BASE_URL = "https://gitlab.com/api/v4/projects"

export class MergeRequestService {
  public get(): any {
    axios.get(`${BASE_URL}/21102706/merge_requests`, {
      headers: {
          'Private-Token': process.env.GITLAB_TOKEN
      }
    }).then(function (x) {
      console.log(x);
    }).catch(function (y) {
        console.log(y);
    });
    return "name"
  }

  public assignReviewer(requestBody: any): void {
    const { object_attributes, project } = requestBody;
    const projectId = project.id;
    const reviewerId = 5828290;
    
    const updateUrl = `${BASE_URL}/${projectId}/merge_requests/${object_attributes.iid}`
    
    if (!object_attributes.work_in_progress) {
      axios.put(updateUrl, { reviewer_id: reviewerId }, {
        headers: {
          'Private-Token': process.env.GITLAB_TOKEN
        }
      }).then(response => console.log(response))
    } else {
      console.log("MR is not ready")
    }
    return object_attributes.iid
  } 
}
