FROM node:14-alpine AS builder

WORKDIR /app

COPY package.json package-lock.json ./
RUN npm install

COPY tsoa.json tsconfig.json ./
COPY src src
RUN npm run build

FROM node:14-alpine

WORKDIR /app
COPY --from=builder /app/node_modules /app/node_modules
COPY --from=builder /app/build /app
COPY package.json .

EXPOSE 5000
CMD ["node", "server.js"]